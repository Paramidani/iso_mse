/*
 * main.c
 *
 *  Created on: 21 Junio 2021
 *      Author: mparamidani
 */

/*==================[inclusions]=============================================*/

#include <string.h>
#include "main.h"
#include "board.h"
#include "os_core.h"
#include "os_apis.h"
#include "os_irq.h"
#include "sapi.h"

/*==================[macros and definitions]=================================*/
#define TEC1_PORT_NUM   0
#define TEC1_BIT_VAL    4

#define TEC2_PORT_NUM   0
#define TEC2_BIT_VAL    8

// Tipo de dato para los eventos de los botones
typedef enum _events_t {
	B_NONE,
	B1_DOWN,
	B1_UP,
	B2_DOWN,
	B2_UP
} events_t;

// Definicion de estados posibles para la FSM de control
typedef enum _state_t {
	INIT,
	GET_DOWN_1,
	GET_DOWN_2,
	GET_UP_1,
	GET_UP_2
} state_t;

// Tipo de datos creado solo por prolijidad
typedef enum _leds_t {
	LED_VERDE,
	LED_ROJO,
	LED_AMARILLO,
	LED_AZUL
} leds_t;

/*==================[Global data declaration]==============================*/

// Estructuras de las tareas implementadas
taskStruct struct_task_control;
taskStruct struct_task_uart;

// Cola para los eventos de los botones
os_queue queue_events;
// Cola para enviar datos por uart
os_queue queue_uart;

char msg[QUEUE_HEAP_SIZE];
uint32_t count_t1;
uint32_t count_t2;
events_t event_1, event_2, event_3, event_4;
state_t state;

/*==================[internal functions declaration]=========================*/
// Handlers de las interrupciones
void tecla1_down_ISR(void);
void tecla1_up_ISR(void);
void tecla2_down_ISR(void);
void tecla2_up_ISR(void);

// Utils para el manejo de strings.
char* make_msg(uint8_t color, uint32_t t1, uint32_t t2, char* msg);
char* itoa(int value, char* result, int base);

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/


/*************************************************************************************************
	 *  @brief Funcion de inicializacion del hardware
     *
     *  @details
     *   Inicializa el systick, las interrupciones de GPIO y la uart.
     *
	 *  @param none
	 *
	 *  @return none.
***************************************************************************************************/
static void initHardware(void)  {
	Board_Init();
	SystemCoreClockUpdate();
	SysTick_Config(SystemCoreClock / SYSTICK_TIME_US); //systick 1ms

	// Seteo la interrupcion 0 para el flanco descendente en la tecla 1
	Chip_SCU_GPIOIntPinSel( 0, TEC1_PORT_NUM, TEC1_BIT_VAL );
	Chip_PININT_ClearIntStatus( LPC_GPIO_PIN_INT, PININTCH( 0 ) );
	Chip_PININT_SetPinModeEdge( LPC_GPIO_PIN_INT, PININTCH( 0 ) );
	Chip_PININT_EnableIntLow( LPC_GPIO_PIN_INT, PININTCH( 0 ) );

	// Seteo la interrupcion 1 para el flanco ascendente en la tecla 1
	Chip_SCU_GPIOIntPinSel( 1, TEC1_PORT_NUM, TEC1_BIT_VAL );
	Chip_PININT_ClearIntStatus( LPC_GPIO_PIN_INT, PININTCH( 1 ) );
	Chip_PININT_SetPinModeEdge( LPC_GPIO_PIN_INT, PININTCH( 1 ) );
	Chip_PININT_EnableIntHigh( LPC_GPIO_PIN_INT, PININTCH( 1 ) );

	// Seteo la interrupcion 2 para el flanco descendente en la tecla 2
	Chip_SCU_GPIOIntPinSel( 2, TEC2_PORT_NUM, TEC2_BIT_VAL );
	Chip_PININT_ClearIntStatus( LPC_GPIO_PIN_INT, PININTCH( 2 ) );
	Chip_PININT_SetPinModeEdge( LPC_GPIO_PIN_INT, PININTCH( 2 ) );
	Chip_PININT_EnableIntLow( LPC_GPIO_PIN_INT, PININTCH( 2 ) );

	// Seteo la interrupcion 3 para el flanco ascendente en la tecla 2
	Chip_SCU_GPIOIntPinSel( 3, TEC2_PORT_NUM, TEC2_BIT_VAL );
	Chip_PININT_ClearIntStatus( LPC_GPIO_PIN_INT, PININTCH( 3 ) );
	Chip_PININT_SetPinModeEdge( LPC_GPIO_PIN_INT, PININTCH( 3 ) );
	Chip_PININT_EnableIntHigh( LPC_GPIO_PIN_INT, PININTCH( 3 ) );

	/* Inicializar UART_USB a 115200 baudios */
	uartConfig( UART_USB, 115200 );
}

/*************************************************************************************************
	 *  @brief Sobreescritura del hook de tick de sistema
     *
     *  @details
     *   Se ejecuta cada vez que se produce un tick de sistema. Es llamada desde el handler de
     *   SysTick.
     *   Aumenta los contadores de t1 y t2 dependiendo del estado de la FSM.
     *
	 *  @param none
	 *
	 *  @return none.
***************************************************************************************************/
void tickHook(void)  {
	if (state==GET_DOWN_1) {
		count_t1++;
	}
	if (state==GET_UP_1) {
		count_t2++;
	}
}

/*************************************************************************************************
	 *  @brief Handler de la interrupcion del flanco descendente del boton 1.
     *
     *  @details
     *   Envia a la cola queue_events un evento del tipo B1_DOWN.
     *
	 *  @param none
	 *
	 *  @return none.
***************************************************************************************************/
void tecla1_down_ISR(void) {
	events_t ev = B1_DOWN;
	os_QueueSend(&queue_events, &ev);
	Chip_PININT_ClearIntStatus( LPC_GPIO_PIN_INT, PININTCH( 0 ) );
}

/*************************************************************************************************
	 *  @brief Handler de la interrupcion del flanco ascendente del boton 1.
     *
     *  @details
     *   Envia a la cola queue_events un evento del tipo B1_UP.
     *
	 *  @param none
	 *
	 *  @return none.
***************************************************************************************************/
void tecla1_up_ISR(void) {
	events_t ev = B1_UP;
	os_QueueSend(&queue_events, &ev);
	Chip_PININT_ClearIntStatus( LPC_GPIO_PIN_INT, PININTCH( 1 ) );
}

/*************************************************************************************************
	 *  @brief Handler de la interrupcion del flanco descendente del boton 2.
     *
     *  @details
     *   Envia a la cola queue_events un evento del tipo B2_DOWN.
     *
	 *  @param none
	 *
	 *  @return none.
***************************************************************************************************/
void tecla2_down_ISR(void) {
	events_t ev = B2_DOWN;
	os_QueueSend(&queue_events, &ev);
	Chip_PININT_ClearIntStatus( LPC_GPIO_PIN_INT, PININTCH( 2 ) );
}

/*************************************************************************************************
	 *  @brief Handler de la interrupcion del flanco ascendente del boton 2.
     *
     *  @details
     *   Envia a la cola queue_events un evento del tipo B2_UP.
     *
	 *  @param none
	 *
	 *  @return none.
***************************************************************************************************/
void tecla2_up_ISR(void) {
	events_t ev = B2_UP;
	os_QueueSend(&queue_events, &ev);
	Chip_PININT_ClearIntStatus( LPC_GPIO_PIN_INT, PININTCH( 3 ) );
}

/*************************************************************************************************
	 *  @brief Funcion para armar el string a enviar por uart.
     *
     *  @details
     *   Recibe el color de led y los tiempos t1 y t2 y guarda en *msg el string a enviar
     *
	 *  @param 	led_sel 	Color del led encendido
	 *  		t1 			Tiempo t1 en ms
	 *  		t2			Tiempo t2 en ms
	 *  		msg 		Puntero al string del mensaje
	 *
	 *  @return char *msg 	Puntero al string del mensaje
***************************************************************************************************/
char* make_msg(leds_t led_sel, uint32_t t1, uint32_t t2, char* msg) {
	char s_t1[12];
    char s_t2[12];
    char s_ttotal[13];

    char header_tot[]="\t Tiempo encendido: ";
    char header_t1[]="\t Tiempo entre flancos descendentes: ";
    char header_t2[]="\t Tiempo entre flancos ascendentes: ";
    char s_ms[]=" ms\n\r";

    itoa(t1,s_t1,10);
    strcat(s_t1,s_ms);
    itoa(t2,s_t2,10);
    strcat(s_t2,s_ms);
    itoa((t1+t2),s_ttotal,10);
    strcat(s_ttotal,s_ms);

    strcpy(msg,"Led ");
    switch(led_sel) {
    	case LED_VERDE:
    	    strcat(msg,"VERDE encendido:\n\r");
    	    break;
    	case LED_ROJO:
    	    strcat(msg,"ROJO encendido:\n\r");
    	    break;
    	case LED_AMARILLO:
    	    strcat(msg,"AMARILLO encendido:\n\r");
    	    break;
    	case LED_AZUL:
    	    strcat(msg,"AZUL encendido:\n\r");
    	    break;
    }
    strcat(msg,header_tot);
    strcat(msg,s_ttotal);
    strcat(msg,header_t1);
    strcat(msg,s_t1);
    strcat(msg,header_t2);
    strcat(msg,s_t2);

    return msg;
}

/*************************************************************************************************
	 *  @brief Funcion para convertir un entero a codigo ascci.
     *
     *  @details
     *   Recibe el valor entero y la base de conversion y lo transforma a una cadena de caracteres.
     *
	 *  @param 	value 		Valor entero a convertir
	 *  		result		Puntero al string resultado
	 *  		base 		Base del entero, debe ser mayor o igual a 2 y menor o igual que 36.
	 *
	 *  @return result		Puntero al string resultado
***************************************************************************************************/
char* itoa(int value, char* result, int base) {
   // check that the base if valid
   if (base < 2 || base > 36) { *result = '\0'; return result; }

   char* ptr = result, *ptr1 = result, tmp_char;
   int tmp_value;

   do {
      tmp_value = value;
      value /= base;
      *ptr++ = "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz" [35 + (tmp_value - value * base)];
   } while ( value );

   // Apply negative sign
   if (tmp_value < 0) *ptr++ = '-';
   *ptr-- = '\0';
   while(ptr1 < ptr) {
      tmp_char = *ptr;
      *ptr--= *ptr1;
      *ptr1++ = tmp_char;
   }
   return result;
}


/*==================[Definicion de tareas para el OS]==========================*/

/*************************************************************************************************
	 *  @brief Tarea principal, implementa la maquina de estados de control
     *
     *  @details
     *   Recibe por queue_events los eventos de los botones y busca secuencias validas, si se obtiene
     *   una secuencia invalida se reseta la maquina de estados.
     *   Resetea y lee los valores de los contadores de t1 y t2 cuando corresponde.
     *   Controla el encendido y apagado de los leds.
     *   Arma los mensajes a enviar por uart y los agrega a la cola queue_uart.
     *   Posee prioridad mas alta que el resto de las tareas.
     *
	 *  @param none
	 *
	 *  @return none.
***************************************************************************************************/
void task_control(void) {
    uint32_t t1_int, t2_int;
    uint32_t i;

    while (1) {
    	switch (state) {
    		case INIT :
    			os_QueueReceive(&queue_events,&event_1);
    			if (event_1 == B1_DOWN || event_1 == B2_DOWN) {
    				count_t1 = 0;
    				state = GET_DOWN_1;
    			}
    			else {
    				count_t1 = 0;
    				event_1 = B_NONE;
    				state = INIT;
    			}
    			break;
    		case GET_DOWN_1 :
    			os_QueueReceive(&queue_events,&event_2);
    			if ((event_1 == B1_DOWN && event_2 == B2_DOWN) || (event_1 == B2_DOWN && event_2 == B1_DOWN)) {
    				t1_int = count_t1;
    				state = GET_DOWN_2;
    			}
    			else {
    				count_t1 = 0;
    				event_2 = B_NONE;
    				state = INIT;
    			}
    			break;
    		case GET_DOWN_2 :
    			os_QueueReceive(&queue_events,&event_3);
    			if (event_3 == B1_UP || event_3 == B2_UP) {
    				count_t2 = 0;
    				state = GET_UP_1;
    			}
    			else {
    				count_t2 = 0;
    				event_3 = B_NONE;
    				state = INIT;
    			}
    			break;
    		case GET_UP_1 :
    			os_QueueReceive(&queue_events,&event_4);
    			if ((event_3 == B1_UP && event_4 == B2_UP) || (event_3 == B2_UP && event_4 == B1_UP)) {
    				t2_int = count_t2;
    				state = GET_UP_2;
    			}
    			else {
    				count_t2 = 0;
    				event_4 = B_NONE;
    				state = INIT;
    			}
    			break;
    		case GET_UP_2 :
    	        // Caso 1: Led verde - El boton 1 desciende y asciende primero (led 3)
    	        if (event_1==B1_DOWN && event_3==B1_UP) {
    	        	//msg = make_msg(LED_VERDE, t1_int, t2_int); // Armo el string
    	        	make_msg(LED_VERDE, t1_int, t2_int, msg);
    	    		i = 0;
    	    		while(msg[i] != NULL)  {
    	    			os_QueueSend(&queue_uart,(msg + i)); // Envio a la cola de la uart
    	    			i++;
    	    		}
    	        	gpioWrite(LED3,true); // Prendo el led
    	        	os_ApiDelay(t1_int+t2_int); // Delay de t1+t2
    	        	gpioWrite(LED3,false);// Apago el led
    	        }
    	        // Caso 2: Led rojo - El boton 1 desciende primero y el boton 2 asciende primero (led 1)
    	        if (event_1==B1_DOWN && event_3==B2_UP) {
    	        	make_msg(LED_ROJO, t1_int, t2_int, msg);
    	    		i = 0;
    	    		while(msg[i] != NULL)  {
    	    			os_QueueSend(&queue_uart,(msg + i)); // Envio a la cola de la uart
    	    			i++;
    	    		}
    	        	gpioWrite(LED2,true); // Prendo el led
    	        	os_ApiDelay(t1_int+t2_int); // Delay de t1+t2
    	        	gpioWrite(LED2,false);// Apago el led
    	        }
    	        // Caso 3: Led amarillo - El boton 2 desciende primero y el boton 1 asciende primero (led 2)
    	        if (event_1==B2_DOWN && event_3==B1_UP) {
    	        	make_msg(LED_AMARILLO, t1_int, t2_int, msg);
    	    		i = 0;
    	    		while(msg[i] != NULL)  {
    	    			os_QueueSend(&queue_uart,(msg + i)); // Envio a la cola de la uart
    	    			i++;
    	    		}
    	        	gpioWrite(LED1,true); // Prendo el led
    	        	os_ApiDelay(t1_int+t2_int); // Delay de t1+t2
    	        	gpioWrite(LED1,false);// Apago el led
    	        }
    	        // Caso 4: Led azul - El boton 2 desciende y asciende primero (led rgb)
    	        if (event_1==B2_DOWN && event_3==B2_UP) {
    	        	make_msg(LED_AZUL, t1_int, t2_int, msg);
    	    		i = 0;
    	    		while(msg[i] != NULL)  {
    	    			os_QueueSend(&queue_uart,(msg + i)); // Envio a la cola de la uart
    	    			i++;
    	    		}
    	        	gpioWrite(LEDB,true); // Prendo el led
    	        	os_ApiDelay(t1_int+t2_int); // Delay de t1+t2
    	        	gpioWrite(LEDB,false);// Apago el led
    	        }
    			event_1 = B_NONE;
    			event_2 = B_NONE;
    			event_3 = B_NONE;
    			event_4 = B_NONE;
    			state = INIT;
    			count_t1 = 0;
    			count_t2 = 0;
    			break;
    		default :
    			event_1 = B_NONE;
    			event_2 = B_NONE;
    			event_3 = B_NONE;
    			event_4 = B_NONE;
    			state = INIT;
    			count_t1 = 0;
    			count_t2 = 0;
    			break;
    	}
    }
}

/*************************************************************************************************
	 *  @brief 	Tarea encargada de enviar los datos por uart.
     *
     *  @details
     *   Recibe a traves e queue_uart los datos a enviar. Tiene prioridad mas baja que la tarea
     *   de control.
     *
	 *  @param none
	 *
	 *  @return none.
***************************************************************************************************/
void task_uart(void)  {
	char aux;

	while(1)  {
		os_QueueReceive(&queue_uart,&aux);
		uartWriteByte(UART_USB,aux);
	}
}


/*============================================================================*/

int main(void)  {

    char name_task_control[] = "task_control";
    char name_task_uart[] = "task_uart";

	initHardware();

	if(!os_attachIrq(PIN_INT0_IRQn,tecla1_down_ISR)) {
		os_setError(ERR_OS_IRQ_HANDLER,main);
	}
	if(!os_attachIrq(PIN_INT1_IRQn,tecla1_up_ISR)) {
		os_setError(ERR_OS_IRQ_HANDLER,main);
	}
	if(!os_attachIrq(PIN_INT2_IRQn,tecla2_down_ISR)) {
		os_setError(ERR_OS_IRQ_HANDLER,main);
	}
	if(!os_attachIrq(PIN_INT3_IRQn,tecla2_up_ISR)) {
		os_setError(ERR_OS_IRQ_HANDLER,main);
	}

	os_init();

    // Tarea principal: consiste en una maquina de estados que detecta la secuencia ingresada.
	// Controla los contadores t1 y t2, manjea los leds y envia los datos a la cola de la uart.
	os_initTask(task_control, name_task_control, TASK_PRIORITY_HIGH, &struct_task_control);

	// Tarea encargada de enviar los datos por uart.
	// Recibe a traves e queue_uart los datos a enviar.
	os_initTask(task_uart, name_task_uart, TASK_PRIORITY_LOW, &struct_task_uart);

	// Inicializacion de colas
    os_QueueInit(&queue_events, sizeof(events_t),(QUEUE_HEAP_SIZE/sizeof(events_t)));
    os_QueueInit(&queue_uart, sizeof(char), QUEUE_HEAP_SIZE);

	event_1 = B_NONE;
	event_2 = B_NONE;
	event_3 = B_NONE;
	event_4 = B_NONE;
	state = INIT;
    count_t1 = 0;
    count_t2 = 0;

	while (1) {
	}
}

/** @} doxygen end group definition */

/*==================[end of file]============================================*/
