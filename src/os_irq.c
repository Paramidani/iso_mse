/*
 * os_irq.c
 *
 *  Created on: Aug 16, 2021
 *      Author: mparamidani
 */

#include "os_irq.h"

static usr_irq_handler_t usr_isr_vector[N_IRQ]; // Vector de punteros a handlers de interrupciones del usuario

/*************************************************************************************************
	 *  @brief API del OS para habilitar una interrupcion.
     *
     *  @details
     *   Si la interrupcion no fue definida anteriormente por el usuario se carga el puntero
     *   al handler en el vector y se habilita la interrupcion.
     *
	 *  @param  irq_n 			Numero de interrupcion a agregar
	 *   		usr_irq_handler Puntero al handler de usuario de la interrupcion.
	 *  @return bool 			Devuelve true si pudo agregar la irq y false sino pudo.
***************************************************************************************************/
bool os_attachIrq(LPC43XX_IRQn_Type irq_n, usr_irq_handler_t usr_irq_handler) {

	if (usr_isr_vector[irq_n] == NULL) {
		usr_isr_vector[irq_n] = usr_irq_handler;
		NVIC_ClearPendingIRQ(irq_n);
		NVIC_EnableIRQ(irq_n);
		return true;
	}
	return false;
}

/*************************************************************************************************
	 *  @brief API del OS para deshabilitar una interrupcion.
     *
     *  @details
     *   Si la interrupcion fue definida anteriormente por el usuario se elimina el puntero
     *   al handler del vector y se deshabilita la interrupcion.
     *
	 *  @param  irq_n 			Numero de la interrupcion a deshabilitar.
	 *  @return bool 			Devuelve true si pudo deshabilitar la irq y false sino pudo.
***************************************************************************************************/
bool os_detachIrq(LPC43XX_IRQn_Type irq_n) {

	if (usr_isr_vector[irq_n] != NULL) {
		usr_isr_vector[irq_n] = NULL;
		NVIC_ClearPendingIRQ(irq_n);
		NVIC_DisableIRQ(irq_n);
		return true;
	}
	return false;
}

/*************************************************************************************************
	 *  @brief Handler del sistema operativo para todas las interrupciones.
     *
     *  @details
     *   Se guarda el estado del OS al momento que ocurrio la interrupcion y se carga como nuevo
     *   estado OS_IRQ_RUN.
     *   Se ejecuta el handler de la interrupcion si es que esta configurado, sino se reporta
     *   un error del tipo ERR_OS_IRQ_HANDLER.
     *   Se limpia el flag de la interrupcion luego de atenderla.
     *   Se vuelve a cargar el estado previo del OS.
     *   Si hubo alguna llamada desde una interrupcion a una api liberando un evento se llama
     *   al scheduler.
     *
	 *  @param  irq_n 			Numero de la interrupcion a atender.
	 *  @return none
***************************************************************************************************/
static void os_irqHandler(LPC43XX_IRQn_Type irq_n)  {
	osState prev_os_state;

	// Se guarda el estado del sistema operativo antes de atender la interrupcion.
	prev_os_state = os_getState();

	// Se cambia el estado del sistema operativo a OS_IRQ_RUN
	os_setState(OS_IRQ_RUN);

	if (usr_isr_vector[irq_n] != NULL) {
		usr_isr_vector[irq_n]();
	}
	else {
		// Se reporta el error
		os_setError(ERR_OS_IRQ_HANDLER,os_irqHandler);
	}

	// Se vuelve a configurar el estado previo del sistema operativo
	os_setState(prev_os_state);

	// Se limpia el flag de la interrupcion luego de ser atendida
	NVIC_ClearPendingIRQ(irq_n);

	//Si hubo alguna llamada desde una interrupcion a una api liberando un evento, entonces se llama al scheduler
	if (os_getScheduleFromISR())  {
		os_setScheduleFromISR(false);
		os_forceSchedule();
	}
}

/*==================[interrupt service routines]=============================*/

void DAC_IRQHandler(void){os_irqHandler(         DAC_IRQn         );}
void M0APP_IRQHandler(void){os_irqHandler(       M0APP_IRQn       );}
void DMA_IRQHandler(void){os_irqHandler(         DMA_IRQn         );}
void FLASH_EEPROM_IRQHandler(void){os_irqHandler(RESERVED1_IRQn   );}
void ETH_IRQHandler(void){os_irqHandler(         ETHERNET_IRQn    );}
void SDIO_IRQHandler(void){os_irqHandler(        SDIO_IRQn        );}
void LCD_IRQHandler(void){os_irqHandler(         LCD_IRQn         );}
void USB0_IRQHandler(void){os_irqHandler(        USB0_IRQn        );}
void USB1_IRQHandler(void){os_irqHandler(        USB1_IRQn        );}
void SCT_IRQHandler(void){os_irqHandler(         SCT_IRQn         );}
void RIT_IRQHandler(void){os_irqHandler(         RITIMER_IRQn     );}
void TIMER0_IRQHandler(void){os_irqHandler(      TIMER0_IRQn      );}
void TIMER1_IRQHandler(void){os_irqHandler(      TIMER1_IRQn      );}
void TIMER2_IRQHandler(void){os_irqHandler(      TIMER2_IRQn      );}
void TIMER3_IRQHandler(void){os_irqHandler(      TIMER3_IRQn      );}
void MCPWM_IRQHandler(void){os_irqHandler(       MCPWM_IRQn       );}
void ADC0_IRQHandler(void){os_irqHandler(        ADC0_IRQn        );}
void I2C0_IRQHandler(void){os_irqHandler(        I2C0_IRQn        );}
void SPI_IRQHandler(void){os_irqHandler(         I2C1_IRQn        );}
void I2C1_IRQHandler(void){os_irqHandler(        SPI_INT_IRQn     );}
void ADC1_IRQHandler(void){os_irqHandler(        ADC1_IRQn        );}
void SSP0_IRQHandler(void){os_irqHandler(        SSP0_IRQn        );}
void SSP1_IRQHandler(void){os_irqHandler(        SSP1_IRQn        );}
void UART0_IRQHandler(void){os_irqHandler(       USART0_IRQn      );}
void UART1_IRQHandler(void){os_irqHandler(       UART1_IRQn       );}
void UART2_IRQHandler(void){os_irqHandler(       USART2_IRQn      );}
void UART3_IRQHandler(void){os_irqHandler(       USART3_IRQn      );}
void I2S0_IRQHandler(void){os_irqHandler(        I2S0_IRQn        );}
void I2S1_IRQHandler(void){os_irqHandler(        I2S1_IRQn        );}
void SPIFI_IRQHandler(void){os_irqHandler(       RESERVED4_IRQn   );}
void SGPIO_IRQHandler(void){os_irqHandler(       SGPIO_INT_IRQn   );}
void GPIO0_IRQHandler(void){os_irqHandler(       PIN_INT0_IRQn    );}
void GPIO1_IRQHandler(void){os_irqHandler(       PIN_INT1_IRQn    );}
void GPIO2_IRQHandler(void){os_irqHandler(       PIN_INT2_IRQn    );}
void GPIO3_IRQHandler(void){os_irqHandler(       PIN_INT3_IRQn    );}
void GPIO4_IRQHandler(void){os_irqHandler(       PIN_INT4_IRQn    );}
void GPIO5_IRQHandler(void){os_irqHandler(       PIN_INT5_IRQn    );}
void GPIO6_IRQHandler(void){os_irqHandler(       PIN_INT6_IRQn    );}
void GPIO7_IRQHandler(void){os_irqHandler(       PIN_INT7_IRQn    );}
void GINT0_IRQHandler(void){os_irqHandler(       GINT0_IRQn       );}
void GINT1_IRQHandler(void){os_irqHandler(       GINT1_IRQn       );}
void EVRT_IRQHandler(void){os_irqHandler(        EVENTROUTER_IRQn );}
void CAN1_IRQHandler(void){os_irqHandler(        C_CAN1_IRQn      );}
void ADCHS_IRQHandler(void){os_irqHandler(       ADCHS_IRQn       );}
void ATIMER_IRQHandler(void){os_irqHandler(      ATIMER_IRQn      );}
void RTC_IRQHandler(void){os_irqHandler(         RTC_IRQn         );}
void WDT_IRQHandler(void){os_irqHandler(         WWDT_IRQn        );}
void M0SUB_IRQHandler(void){os_irqHandler(       M0SUB_IRQn       );}
void CAN0_IRQHandler(void){os_irqHandler(        C_CAN0_IRQn      );}
void QEI_IRQHandler(void){os_irqHandler(         QEI_IRQn         );}

