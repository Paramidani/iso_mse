/*
 * os_core.c
 *
 *  Created on: 21 Junio 2021
 *      Author: mparamidani
 */

#include "os_core.h"

/*==================[definicion de variables globales]=================================*/
static osControl os_control;
static taskStruct struct_task_idle;
static taskStruct* tasks_ready_list[N_PRIORITIES][MAX_TASK_COUNT];
static uint8_t tasks_ready_count[N_PRIORITIES];
//----------------------------------------------------------------------------------

/*==================[definicion de prototipos static]=================================*/
static void initIdleTask(void);
static void scheduler(void);
static void setPendSV(void);
static void tasks_delay_update(void);
void add_ready_task(taskStruct* t);
static void remove_ready_task(taskStruct* t);

/*==================[definicion de hooks debiles]=================================*/
/*************************************************************************************************
	 *  @brief Hook de retorno de tareas
     *
     *  @details
     *   Esta funcion no deberia accederse bajo ningun concepto, porque ninguna tarea del OS
     *   debe retornar. Si lo hace, es un comportamiento anormal y debe ser tratado.
     *
	 *  @param none
	 *
	 *  @return none.
***************************************************************************************************/
void __attribute__((weak)) returnHook(void)  {
	while(1);
}

/*************************************************************************************************
	 *  @brief Hook de tick de sistema
     *
     *  @details
     *   Se ejecuta cada vez que se produce un tick de sistema. Es llamada desde el handler de
     *   SysTick.
     *
	 *  @param none
	 *
	 *  @return none.
	 *
	 *  @warning	Esta funcion debe ser lo mas corta posible porque se ejecuta dentro del handler
     *   			mencionado, por lo que tiene prioridad sobre el cambio de contexto y otras IRQ.
	 *
	 *  @warning 	Esta funcion no debe bajo ninguna circunstancia utilizar APIs del OS dado
	 *  			que podria dar lugar a un nuevo scheduling.
***************************************************************************************************/
void __attribute__((weak)) tickHook(void)  {
	__asm volatile( "nop" );
}

/*************************************************************************************************
	 *  @brief Hook de error de sistema
     *
     *  @details
     *   Esta funcion es llamada en caso de error del sistema, y puede ser utilizada a fin de hacer
     *   debug. El puntero de la funcion que llama a errorHook es pasado como parametro para tener
     *   informacion de quien la esta llamando, y dentro de ella puede verse el codigo de error
     *   en la estructura de control de sistema. Si ha de implementarse por el usuario para manejo
     *   de errores, es importante tener en cuenta que la estructura de control solo esta disponible
     *   dentro de este archivo.
     *
	 *  @param caller		Puntero a la funcion donde fue llamado errorHook. Implementado solo a
	 *  					fines de trazabilidad de errores
	 *
	 *  @return none.
***************************************************************************************************/
void __attribute__((weak)) errorHook(void *caller)  {
	// Revisar el contenido de control_OS.error para obtener informacion. Utilizar os_getError()
	while(1);
}

/*************************************************************************************************
	 *  @brief Tarea Idle (segundo plano)
     *
     *  @details
     *   Esta tarea se ejecuta solamente cuando todas las demas tareas estan en estado bloqueado.
     *   Puede ser redefinida por el usuario.
     *
	 *  @param none
	 *
	 *  @return none.
	 *
	 *  @warning		No debe utilizarse ninguna funcion API del OS dentro de esta funcion. No
	 *  				debe ser causa de un re-scheduling.
***************************************************************************************************/
void __attribute__((weak)) idleTask(void)  {
	while(1)  {
		__WFI();
	}
}

/*==================[definicion de funciones de OS]=================================*/

/*************************************************************************************************
	 *  @brief Inicializa las tareas que correran en el OS.
     *
     *  @details
     *   Inicializa una tarea para que pueda correr en el OS implementado.
     *   Es necesario llamar a esta funcion para cada tarea antes que inicie
     *   el OS.
     *
	 *  @param *entryPoint		Puntero a la tarea que se desea inicializar.
	 *  @param *name 			Puntero al string del nombre de la tarea (maximo MAX_TASK_NAME_LEN caracteres).
	 *  @param priority 		Prioridad de la tarea.
	 *  @param *task			Puntero a la estructura de control que sera utilizada para
	 *  						la tarea que se esta inicializando.
	 *  @return     None.
***************************************************************************************************/
void os_initTask(void *entryPoint, char *name, taskPriority priority, taskStruct *task) {
	static uint8_t id = 0; 	// El id sera correlativo a medida que se generen mas tareas.

	// Se chequea si se llego al nro maximo de tareas permitidas
	// La tarea IDLE se exceptua del conteo de tareas
	if ((os_control.cant_tasks < MAX_TASK_COUNT && priority < TASK_PRIORITY_IDLE) || entryPoint == idleTask) {
		task->stack[STACK_SIZE/4 - XPSR] = INIT_XPSR;				// Necesario para bit thumb.
		task->stack[STACK_SIZE/4 - PC_REG] = (uint32_t)entryPoint;	// Direccion de la tarea (ENTRY_POINT).
		task->stack[STACK_SIZE/4 - LR] = (uint32_t)returnHook;		//Retorno de la tarea (no deberia darse)
		/*
		 * El valor previo de LR (que es EXEC_RETURN en este caso) es necesario dado que
		 * en esta implementacion, se llama a una funcion desde dentro del handler de PendSV
		 * con lo que el valor de LR se modifica por la direccion de retorno para cuando
		 * se termina de ejecutar getContextoSiguiente
		 */
		task->stack[STACK_SIZE/4 - LR_PREV_VALUE] = EXEC_RETURN;

		// Cargo la estructura de la tarea con su configuracion
		task->stack_pointer = (uint32_t) (task->stack + STACK_SIZE/4 - FULL_STACKING_SIZE); // Valor inicial del stack pointer.
		task->entry_point = entryPoint; // Se guarda el entry point de la tarea.
		task->id = id; // El id es el equivale al contador de tareas del OS.
		task->state = TASK_READY; // Todas las tareas se crean en estado READY
		task->priority = priority;
		task->blocked_ticks = 0;
		for (uint8_t i=0;i<MAX_TASK_NAME_LEN;i++) {
			task->name[i] = *(name+i);
		}

		// Se actualiza la lista de tareas del OS
		os_control.tasks_list[id] = task;
		// Se actualiza la cantidad de tareas definidas en el sistema.
		os_control.cant_tasks++;
		// Se incrementa el contador de id, se le otorga un id correlativo a cada tarea inicializada.
		id++;
		// Se agrega la tarea a la lista de tareas en estado TASK_READY
		add_ready_task(task);
	}
	else {
		os_setError(ERR_OS_MAX_TASK,os_initTask);
	}
}


/*************************************************************************************************
	 *  @brief Inicializa el OS.
     *
     *  @details
     *   Inicializa el OS seteando la prioridad de PendSV como la mas baja posible. Es necesario
     *   llamar esta funcion antes de que inicie el sistema. Es recomendable llamarla luego de
     *   inicializar las tareas
     *
	 *  @param 		None.
	 *  @return     None.
***************************************************************************************************/
void os_init(void) {
	/*
	 * Todas las interrupciones tienen prioridad 0 (la maxima) al iniciar la ejecucion. Para que
	 * no se de la condicion de fault mencionada en la teoria, debemos bajar su prioridad en el
	 * NVIC. La cuenta matematica que se observa da la probabilidad mas baja posible.
	 */
	NVIC_SetPriority(PendSV_IRQn, (1 << __NVIC_PRIO_BITS)-1);

	// Se inicializan los contadores de tareas ready en cero y las listas con NULL
	for (int i = TASK_PRIORITY_HIGH; i < TASK_PRIORITY_IDLE ; i++) {
		tasks_ready_count[i] = 0;
		for (int j = 0; i<MAX_TASK_COUNT; i++) {
			tasks_ready_list[i][j] = NULL;
		}
	}
	/*
	 * Es necesaria la inicializacion de la tarea idle, la cual no es visible al usuario
	 * El usuario puede eventualmente poblarla de codigo o redefinirla, pero no debe
	 * inicializarla ni definir una estructura para la misma.
	 */
	initIdleTask();

	/*
	 * Al iniciar el OS se especifica que se encuentra en la primer ejecucion desde un reset.
	 * Este estado es util para cuando se debe ejecutar el primer cambio de contexto. Los
	 * punteros de tarea_actual y tarea_siguiente solo pueden ser determinados por el scheduler
	 */
	os_control.state = OS_FROM_RESET;
	os_control.current_task = NULL;
	os_control.next_task = NULL;
	os_control.scheduler_irq = 0;

	/*
	 * El vector de tareas termina de inicializarse asignando NULL a las posiciones que estan
	 * luego de la ultima tarea. Esta situacion se da cuando se definen menos de 8 tareas.
	 * Estrictamente no existe necesidad de esto, solo es por seguridad.
	 * NOTA: La ultima tarea de este vector sera siempre la tarea idle
	 */
	for (uint8_t i=0; i<MAX_TASK_COUNT_W_IDLE ; i++) {
		if(i>=os_control.cant_tasks)
			os_control.tasks_list[i] = NULL;
	}
}

/*************************************************************************************************
	 *  @brief Setea el codigo de error en la estructura de control del OS.
     *
     *  @details
     *   La estructura de control del OS no es visible al usuario, por lo que se facilita una API
     *   para setear el ultimo codigo de error ocurrido.
     *
	 *  @param 		ID del error.
	 *  @return     none
	 *  @see errorHook
***************************************************************************************************/
void os_setError(int32_t err_type, void* caller) {
	os_control.error = err_type;
	errorHook(caller);
}

/*************************************************************************************************
	 *  @brief Extrae el codigo de error de la estructura de control del OS.
     *
     *  @details
     *   La estructura de control del OS no es visible al usuario, por lo que se facilita una API
     *   para extraer el ultimo codigo de error ocurrido, para su posterior tratamiento. Esta
     *   funcion puede ser utilizada dentro de errorHook
     *
	 *  @param 		None.
	 *  @return     Ultimo error ocurrido dentro del OS.
	 *  @see errorHook
***************************************************************************************************/
int32_t os_getError(void)  {
	return os_control.error;
}

/*************************************************************************************************
	 *  @brief Extrae la cantidad de tareas de la estructura de control del OS.
     *
     *  @details
     *   La estructura de control del OS no es visible al usuario, por lo que se facilita una API
     *   para extraer la cantida de tareas creadas.
     *
	 *  @param 		None.
	 *  @return     Cantidad de tareas creadas en el OS.
***************************************************************************************************/
uint8_t os_getCantTasks(void) {
	return os_control.cant_tasks;
}

/*************************************************************************************************
	 *  @brief Extrae el estado del OS de la estructura de control del mismo.
     *
     *  @details
     *   La estructura de control del OS no es visible al usuario, por lo que se facilita una API
     *   para extraer el estado actual del OS.
     *
	 *  @param 		None.
	 *  @return     Estado actual del OS.
***************************************************************************************************/
osState os_getState(void) {
	return os_control.state;
}

/*************************************************************************************************
	 *  @brief Cambia el estado del OS modificando la estructura de control del mismo.
     *
     *  @details
     *   La estructura de control del OS no es visible al usuario, por lo que se facilita una API
     *   para configurar el estado actual del OS.
     *
	 *  @param 		st 		Nuevo estado del OS
	 *  @return     none
***************************************************************************************************/
void os_setState(osState st) {
	os_control.state = st;
}

/*************************************************************************************************
	 *  @brief Extrae el puntero a la estructura de la tarea que se encuentra en estado running
	 *  desde la estructura de control del OS.
     *
     *  @details
     *   La estructura de control del OS no es visible al usuario, por lo que se facilita una API
     *   para extraer el puntero a la estructura de la tarea actual.
     *
	 *  @param 		None
	 *  @return     Puntero a la estructura de la tarea actual.
***************************************************************************************************/
taskStruct * os_getCurrentTask(void) {
	return os_control.current_task;
}

/*************************************************************************************************
	 *  @brief Extrae el valor del flag scheduler_irq desde la estructura de control del OS.
     *
     *  @details
     *   Devuelve el valor del flag que indica si es necesario un re-scheduling despues de una
     *   interrupcion.
     *
	 *  @param 		None
	 *  @return     Valor del flag scheduler_irq.
***************************************************************************************************/
bool os_getScheduleFromISR(void) {
	return os_control.scheduler_irq;
}

/*************************************************************************************************
	 *  @brief Configura el valor flag scheduler_irq en la estructura de control del OS.
     *
     *  @details
     *   Configura el flag que fuerza el re-scheduling despues de una interrupcion
     *
	 *  @param 		val 	Valor a setear en el flag.
	 *  @return     None
***************************************************************************************************/
void os_setScheduleFromISR(bool val) {
	os_control.scheduler_irq = val;
}

/*************************************************************************************************
	 *  @brief Funcion forzar la ejecucion del scheduler.
     *
     *  @details
     *   En los casos que un delay de una tarea comience a ejecutarse instantes luego de que
     *   ocurriese un scheduling, se despericia mucho tiempo hasta el proximo tick de sistema,
     *   por lo que se fuerza un scheduling y un cambio de contexto si es necesario.
     *
	 *  @param 		none
	 *  @return     none
***************************************************************************************************/
void os_forceSchedule(void) {
	scheduler();
	setPendSV();
}

/*************************************************************************************************
	 *  @brief Marca el inicio de una seccion como seccion critica.
     *
     *  @details
     *   Usando esta API en conjunto con os_exitCritical() se otorga al usuario la capacidad
     *   de definir una zona de codigo como critica. La seccion encerrada entre os_enterCritical()
     *   y os_exitCritical() sera ejecutada de manera atomica y no se produciran cambios de
     *   contexto. Para lograr esto la atencion de interrupciones se deshabilita.
     *   Nota: la seccion critica debe ser lo mas corta posible.
     *
	 *  @param 		None
	 *  @return     None
	 *  @see 		os_exitCritical
***************************************************************************************************/
void os_enterCritical()  {
	__disable_irq();
	os_control.critical_counter++;
}

/*************************************************************************************************
	 *  @brief Marca el final de una seccion como seccion critica.
     *
     *  @details
     *   Usando esta API en conjunto con os_enterCritical() se otorga al usuario la capacidad
     *   de definir una zona de codigo como critica. La seccion encerrada entre os_enterCritical()
     *   y os_exitCritical() sera ejecutada de manera atomica y no se produciran cambios de
     *   contexto. Para lograr esto la atencion de interrupciones se deshabilita.
     *   Nota: la seccion critica debe ser lo mas corta posible.
     *
	 *  @param 		none
	 *  @return     none
	 *  @see 		os_enterCritical
***************************************************************************************************/
void os_exitCritical()  {
	if (--os_control.critical_counter <= 0)  {
		os_control.critical_counter = 0;
		__enable_irq();
	}
}

/*************************************************************************************************
	 *  @brief Funcion que efectua las decisiones de scheduling.
     *
     *  @details
     *   Segun el critero al momento de desarrollo, determina que tarea debe ejecutarse luego, y
     *   por lo tanto provee los punteros correspondientes para el cambio de contexto. Esta
     *   implementacion de scheduler es muy sencilla, del tipo Round-Robin
     *
	 *  @param 		None.
	 *  @return     None.
***************************************************************************************************/
static void scheduler(void) {
	uint8_t p_index = TASK_PRIORITY_HIGH;
	bool exit_sch = false;

	// Si es el primer ingreso desde el ultimo reset se ejecuta la tarea idle.
	if (os_control.state == OS_FROM_RESET) {
		os_control.current_task = &struct_task_idle;
	}
	else {
		// Cambio el estado de la tarea actual y la agrego a la lista de tareas READY si corresponde
		if (os_control.current_task->state == TASK_RUNNING) {
			os_control.current_task->state = TASK_READY;
			if (os_control.current_task->id != struct_task_idle.id) {
				add_ready_task(os_control.current_task);
			}
		}
		while(!exit_sch) {
			// Empiezo a recorrer las listas de tareas en estado READY, comenzando por la de la prioridad mas alta.
			if (tasks_ready_count[p_index]>0) {
				// Si hay alguna tarea en esa lista la asigno como proxima tarea
				os_control.next_task = (taskStruct*) tasks_ready_list[p_index][0];
				// Y la elimino de la lista de tareas en estado READY
				remove_ready_task(os_control.next_task);
				exit_sch = true;
			}
			else {
				if (p_index < (TASK_PRIORITY_IDLE-1)) {
					// Bajamos al siguiente nivel de prioridad.
					p_index ++;
					exit_sch = false;
				}
				else {
					// Si no hay ninguna tarea en estado ready en ninguna de las 4 prioridades permitidas
					// se ejecuta la tareas idle.
					os_control.next_task = &struct_task_idle;
					exit_sch = true;
				}
			}
		}

	}
}

/*************************************************************************************************
	 *  @brief Funcion para determinar el proximo contexto.
     *
     *  @details
     *   Esta funcion obtiene el siguiente contexto a ser cargado. El cambio de contexto se
     *   ejecuta en el handler de PendSV, dentro del cual se llama a esta funcion
     *
	 *  @param 		sp_current	Este valor es una copia del contenido de MSP al momento en
	 *  			que la funcion es invocada.
	 *  @return     El valor a cargar en MSP para apuntar al contexto de la tarea siguiente.
***************************************************************************************************/
uint32_t getNextContext(uint32_t sp_current) {
	uint32_t sp_next;

	/*
	 * En la primera llamada a getNextContext, se designa que la primer tarea a ejecutar sea
	 * la tarea actual, la cual es la primer tarea inicializada y cuyo puntero de estructura fuese
	 * cargado por la funcion scheduler (observar flujo de programa). Como todas las tareas se crean
	 * en estado READY, directamente se cambia a estado RUNNING y se actualiza la variable de estado
	 * de sistema
	 */
	if (os_control.state == OS_FROM_RESET)  {
		sp_next = os_control.current_task->stack_pointer;
		os_control.current_task->state = TASK_RUNNING;
		os_control.state = OS_NORMAL_RUN;
	}

	/*
	 * En el caso que no sea la primera vez que se ejecuta esta funcion, se hace un cambio de contexto
	 * de manera habitual. Se guarda el MSP (sp_actual) en la variable correspondiente de la estructura
	 * de la tarea corriendo actualmente. Ahora que el estado BLOCKED esta implementado, se debe hacer
	 * un assert de si la tarea actual fue expropiada mientras estaba corriendo o si la expropiacion
	 * fue hecha de manera prematura dado que paso a estado BLOCKED. En el segundo caso, solamente
	 * se puede pasar de BLOCKED a READY a partir de un evento.
	 * Se carga en la variable sp_siguiente el stack pointer de la tarea siguiente, que fue definida
	 * por el scheduler. Se actualiza la misma a estado RUNNING y se retorna al handler de PendSV
	 */
	else {
		os_control.current_task->stack_pointer = sp_current;
		sp_next = os_control.next_task->stack_pointer;
		os_control.current_task = os_control.next_task;
		os_control.current_task->state = TASK_RUNNING;
	}

	return sp_next;
}

/*************************************************************************************************
	 *  @brief SysTick Handler.
     *
     *  @details
     *   El handler del Systick no debe estar a la vista del usuario. Dentro se setea como
     *   pendiente la excepcion PendSV.
     *
	 *  @param 		None.
	 *  @return     None.
***************************************************************************************************/
void SysTick_Handler(void) {

	// Se actualiza el estado de los delays de cada tarea
	tasks_delay_update();

	// Se llama al scheduler.
	scheduler();

	// Se realiza el cambio de contexto
	setPendSV();

	//Luego de determinar la tarea siguiente, se ejecuta la funcion tickhook.
	tickHook();
}

/*************************************************************************************************
	 *  @brief Inicializacion de la tarea idle.
     *
     *  @details
     *   Esta funcion es una version reducida de os_initTarea para la tarea idle. Como esta tarea
     *   debe estar siempre presente y el usuario no la inicializa, los argumentos desaparecen
     *   y se toman estructura y entryPoint fijos. Tampoco se contabiliza entre las tareas
     *   disponibles (no se actualiza el contador de cantidad de tareas). El id de esta tarea
     *   se establece como 255 (0xFF) para indicar que es una tarea especial.
     *
	 *  @param 		None
	 *  @return     None
	 *  @see os_InitTarea
***************************************************************************************************/
static void initIdleTask(void)  {
	char name_task_idle[] = "Tarea_idle";
	struct_task_idle.stack[STACK_SIZE/4 - XPSR] = INIT_XPSR;
	struct_task_idle.stack[STACK_SIZE/4 - PC_REG] = (uint32_t)idleTask;
	struct_task_idle.stack[STACK_SIZE/4 - LR] = (uint32_t)returnHook;
	struct_task_idle.stack[STACK_SIZE/4 - LR_PREV_VALUE] = EXEC_RETURN;
	struct_task_idle.stack_pointer = (uint32_t) (struct_task_idle.stack + STACK_SIZE/4 - FULL_STACKING_SIZE);
	struct_task_idle.entry_point = idleTask;
	struct_task_idle.id = 0xFF;
	struct_task_idle.state = TASK_READY;
	struct_task_idle.priority = TASK_PRIORITY_IDLE;
	struct_task_idle.blocked_ticks = 0;
	for (uint8_t i=0;i<MAX_TASK_NAME_LEN;i++) {
		struct_task_idle.name[i] = *(name_task_idle+i);
	}
}

/*************************************************************************************************
	 *  @brief Setea la bandera correspondiente para lanzar PendSV.
     *
     *  @details
     *   Esta funcion simplemente es a efectos de simplificar la lectura del programa. Setea
     *   la bandera comrrespondiente para que se ejucute PendSV
     *
	 *  @param 		None
	 *  @return     None
***************************************************************************************************/
static void setPendSV(void) {
	/**
	 * Se setea el bit correspondiente a la excepcion PendSV
	 */
	SCB->ICSR = SCB_ICSR_PENDSVSET_Msk;

	/**
	 * Instruction Synchronization Barrier; flushes the pipeline and ensures that
	 * all previous instructions are completed before executing new instructions
	 */
	__ISB();

	/**
	 * Data Synchronization Barrier; ensures that all memory accesses are
	 * completed before next instruction is executed
	 */
	__DSB();
}

/*************************************************************************************************
	 *  @brief Funcion que decrementa en uno el delay de cada tarea si su ticks_blocked es mayor
	 *  a cero.
     *
     *  @details
     *   Esta funcion es llamada unicamente por el SysTick_Handler. Se recorre el vector de tareas
     *   y si el campo ticks_blocked de una tarea es mayor a cero se decrementa en uno su valor.
     *   Si luego de decrementar ticks_blocked vale cero, se cambia el estado de la tarea de
     *   TASK_BLOCKED a TASK_READY.
     *
	 *  @param 		None.
	 *  @return     None.
***************************************************************************************************/
static void tasks_delay_update(void) {
	uint8_t i;

	// Recorro toda la lista de tareas
	for (i=0; i<MAX_TASK_COUNT; i++) {
		if ( (os_control.tasks_list[i]->state == TASK_BLOCKED) && (os_control.tasks_list[i]->blocked_ticks > 0))
		{
			// Si alguna esta bloqueada y con un nomero de blocked_ticks mayor a cero, disminuyo sus blocked_ticks
			os_control.tasks_list[i]->blocked_ticks--;
			if (os_control.tasks_list[i]->blocked_ticks == 0) {
				// Si al disminuit el blocked_ticks este llega a cero, paso la tarea ready
				os_control.tasks_list[i]->state = TASK_READY;
				// Y la agrego a la lista de tareas en estado ready
				add_ready_task(os_control.tasks_list[i]);
			}
		}
	}
}

/*************************************************************************************************
	 *  @brief Funcion para agregar una tarea a la lista de tareas en estado TASK_READY
     *
     *  @details
     *   Primero verifica que el estado de la tarea sea TASK_READY, si no es asi genera
     *   un error del tipo ERR_OS_READY_LIST. Si el estado es TASK_READY, agrega el puntero a la
     *   estructura de la tarea en la tabla correspondiente segun su prioridad y aumenta el
     *   contador asociado en uno.
     *
	 *  @param 		taskStruct* t puntero a la estructura de la tarea a agregar.
	 *  @return     none
***************************************************************************************************/
void add_ready_task(taskStruct* t) {
	if (t->state==TASK_READY) {
		tasks_ready_list[t->priority][tasks_ready_count[t->priority]] = t;
		tasks_ready_count[t->priority]++;
	}
	else {
		os_setError(ERR_OS_READY_LIST,add_ready_task);
	}
}

/*************************************************************************************************
	 *  @brief Funcion para remover una tarea de la lista de tareas en estado TASK_READY
     *
     *  @details
     *   Elimina el puntero a la estructura de la tarea de la tabla correspondiente segun su
     *   prioridad y decrementa el contador asociado en uno. Si la tarea no se encuentra en la
     *   lista se genera un error del tipo ERR_OS_READY_LIST.
     *
	 *  @param 		taskStruct* t puntero a la estructura de la tarea a remover.
	 *  @return     none
***************************************************************************************************/
static void remove_ready_task(taskStruct* t) {
	taskPriority p;
	int8_t index;

	p = t->priority;

	// Primero se obtiene el indice de la tarea a remover de la lista correspondiente a su prioridad
	for (int8_t i = 0; i<tasks_ready_count[p]; i++) {
		if (tasks_ready_list[p][i]->id == t->id) {
			index = i;
			break;
		}
		else {
			index = (int8_t)(-1);
		}
	}
	// Si no se encontro la tarea en la lista se genera un error
	if (index == -1) {
		os_setError(ERR_OS_READY_LIST,remove_ready_task);
	}
	// Si la tarea no es la ultima de la lista es necesario reordenar el resto
	if (index < tasks_ready_count[p]) {
		for (int j=0; j<tasks_ready_count[p]-1; j++) {
			tasks_ready_list[p][j] = tasks_ready_list[p][j+1];
		}
	}
	// Se pone en NULL la ultima posicion de la lista, ahora vacia
	tasks_ready_list[p][tasks_ready_count[p]-1] = NULL;
	// Se decrementa el contador correspondiente
	tasks_ready_count[p]--;
}
