/*
 * os_apis.c
 *
 *  Created on: Aug 2, 2021
 *      Author: mparamidani
 */

#include "os_apis.h"

/*==================[definicion de variables globales]=================================*/

/*==================[definicion de funciones APIs del OS]==============================*/

/*************************************************************************************************
	 *  @brief API del OS para generar un delay en la tarea actual.
     *
     *  @details
     *   La funcion obtiene la tarea actual y si esta en estado running le asigna la cantidad de
     *   ticks ingresados. Como el systick time esta configurado en 1ms ticks y milisegundos son
     *   numeros equivalentes. Luego de configurar el delay a la tarea, esta se pasa a estado
     *   blocked y se fuerza un schedule.
     *
	 *  @param none
	 *  @return none
	 *  @warning	No puede ser llamada desde un handler de interrupcion,produce un error del OS
	 *  			del tipo ERR_OS_DELAY_FROM_ISR.
***************************************************************************************************/
void os_ApiDelay(uint32_t time_ms) {
	taskStruct * current_task;

	// os_ApiDelay no puede llamarse desde el handler de una interrupcion.
	if(os_getState() == OS_IRQ_RUN)  {
		os_setError(ERR_OS_DELAY_FROM_ISR, os_ApiDelay);
	}

	os_enterCritical();
    // -----------------------------------------------------------------------------
	// Inicio de seccion critica
    // -----------------------------------------------------------------------------
	current_task = os_getCurrentTask();

	if (current_task->state == TASK_RUNNING && time_ms > 0) {
		// Se asigna el tiempo pasado como argumento a la cantidad de ticks bloqueados de la tarea
		// En este caso el tiempo de Systick es 1ms, por lo tanto ticks y ms son equivalentes.
		current_task->blocked_ticks = time_ms;
		// Se pasa la tarea a estado bloqueado
		current_task->state = TASK_BLOCKED;
		// -----------------------------------------------------------------------------
		// Fin de seccion critica
		// -----------------------------------------------------------------------------
		os_exitCritical();
		// Se fuerza un rescheduling del sistema
		os_forceSchedule();
	}
	else {
		// -----------------------------------------------------------------------------
		// Fin de seccion critica
		// -----------------------------------------------------------------------------
		os_exitCritical();
	}
}


/*************************************************************************************************
	 *  @brief Funcion de inicializacion de un semaforo binario
     *
     *  @details
     *   Antes de utilizar cualquier semaforo binario este debe ser inicializado.
     *   Todos los semaforos se inicializan tomados y sin bloquear ninguna tarea.
     *
	 *  @param		sem		Semaforo a inicializar
	 *  @return     none
***************************************************************************************************/
void os_SemaphoreInit(os_semaphore* sem) {
	sem->taken = true;
	sem->blocked_task = NULL;
}


/*************************************************************************************************
	 *  @brief Funcion para tomar un semaforo binario.
     *
     *  @details
     *   Esta funcion es utilizada para tomar un semaforo binario.
     *   Si el semaforo no se encuentra disponible la tarea es bloqueada y se guarda su puntero
     *   dentro de la estructura del semaforo.
     *
	 *  @param		sem		Semaforo a tomar
	 *  @return     none
	 *  @warning 	Esta funcion no puede ser llamada desde el handler de una interrupcion.
	 *  			Si se hace produce un error de OS del tipo ERR_OS_SEMTAKE_FROM_ISR.
***************************************************************************************************/
void os_SemaphoreTake(os_semaphore* sem) {
	bool exit_loop = false;
	taskStruct * current_task;

	// Esta funcion no puede ser llamada desde el handler de una interrupcion.
	if(os_getState() == OS_IRQ_RUN)  {
		os_setError(ERR_OS_SEMTAKE_FROM_ISR, os_SemaphoreTake);
	}

	while (!exit_loop) {
		// Si el semaforo esta tomado se bloquea la tarea
		if(sem->taken)  {
			os_enterCritical();
		    // -----------------------------------------------------------------------------
			// Inicio de seccion critica
		    // -----------------------------------------------------------------------------
			current_task = os_getCurrentTask();
			current_task->state = TASK_BLOCKED;
			sem->blocked_task = current_task; // El puntero a la estructura de la tarea bloqueada se guarda dentro de la estructura del semaforo
			// -----------------------------------------------------------------------------
			os_exitCritical();
			// Se fuerza un rescheduling del sistema
			os_forceSchedule();
		}
		// Si el semaforo esta libre se lo marca como tomado
		else  {
			sem->taken = true;
			exit_loop = true;
		}
	}
}


/********************************************************************************
	 *  @brief Funcion para liberar un semaforo binario
     *
     *  @details
     *   Esta funcion es utilizada para liberar un semaforo binario.
     *   Si el semaforo estaba bloqueando una tarea, esta se pasa al estado ready.
     *
	 *  @param		sem		Semaforo a liberar
	 *  @return     none
 *******************************************************************************/
void os_SemaphoreGive(os_semaphore* sem) {
	taskStruct * current_task;

	current_task = os_getCurrentTask();

	if (current_task->state == TASK_RUNNING && sem->taken == true) {
		sem->taken = false;
		if (sem->blocked_task != NULL)  {
			sem->blocked_task->state = TASK_READY;
			add_ready_task(sem->blocked_task);
			// Si la API fue llamada desde una interrupcion se debe forzar un re-schedule
			if (os_getState() == OS_IRQ_RUN)
				os_setScheduleFromISR(true);
		}
	}
}

/*************************************************************************************************
	 *  @brief Funcion para iniciar una cola
     *
     *  @details
     *   Primero se verifica que la cantidad de elementos multiplicada por el tamano de cada elemento
     *   sea menor que el QUEUE_HEAP_SIZE. Si esto se cumple se inicializa la estructura de la cola.
     *   Si no se cumple, se reporta el error.
     *
	 *  @param		q 				Puntero a la estructura de la cola que se quiere inicializar.
	 *              element_size 	Tamano de cada elemento de la cola.
	 *              queue_size      Cantidad de elementos maxima permitida.
	 *  @return     None.
	 *
	 *  @warning	Esta inicializacion fija el tamaño de cada elemento, por lo que no vuelve
	 *  			a consultarse en otras funciones, pasar datos con otros tamaños en funciones
	 *  			de escritura y lectura puede dar lugar a corrupcion de datos.
***************************************************************************************************/
void os_QueueInit(os_queue* q, uint16_t element_size, uint16_t queue_size) {
	if ((queue_size * element_size)>QUEUE_HEAP_SIZE) {
		os_setError(ERR_OS_QUEUE,os_QueueInit);
	}
	else {
		q->index_head = 0;
		q->index_tail = 0;
		q->blocked_task = NULL;
		q->element_size = element_size;
		q->queue_size = queue_size;
	}
}

/*************************************************************************************************
	 *  @brief Funcion para enviar un dato a una cola
     *
     *  @details
     *   Si la cola estaba bloqueando una tarea, esta pasa al estado ready.
     *   Si la cola esta llena, la tarea que intenta enviar un dato se bloquea.
     *   Si la cola no esta llena, se guarda el item y se aumenta el indice de la cola.
     *
	 *  @param		q 		Puntero a la estructura de la cola a escribir.
	 *  			item 	Dato a escribir en la cola.
	 *  @return     none.
***************************************************************************************************/
void os_QueueSend(os_queue* q, void* item) {
	uint16_t index;
	taskStruct * current_task;

	index = q->index_head * q->element_size;

	// Si la cola estaba vacia y habia una tarea bloqueada se la pasa a estado ready
	if ((q->index_head==q->index_tail && q->blocked_task!=NULL) && (q->blocked_task->state==TASK_BLOCKED)) {
		q->blocked_task->state = TASK_READY;
		add_ready_task(q->blocked_task);
		q->blocked_task = NULL;
		// Si la API fue llamada desde una interrupcion se debe forzar un re-schedule
		if (os_getState() == OS_IRQ_RUN)
			os_setScheduleFromISR(true);
	}
	// Si se quiere escribir una cola desde un ISR y esta llena, la operacion es abortada (no se puede bloquear un handler)
	if ((os_getState() == OS_IRQ_RUN) && (((q->index_head +1) % q->queue_size) == q->index_tail)) {
		os_setError(ERR_OS_QUEUE_FULL_ISR, os_QueueSend);
		return;
	}

	// Si la cola esta llena, la tarea actual se bloquea
	while (((q->index_head +1) % q->queue_size) == q->index_tail) {
		os_enterCritical();
	    // -----------------------------------------------------------------------------
		// Inicio de seccion critica
	    // -----------------------------------------------------------------------------
		current_task = os_getCurrentTask();
		current_task->state = TASK_BLOCKED;
		q->blocked_task = current_task; // El puntero a la estructura de la tarea bloqueada se guarda dentro de la estructura de la cola
		// -----------------------------------------------------------------------------
		os_exitCritical();
		// Se fuerza un rescheduling del sistema
		os_forceSchedule();
	}
	// Si hay lugar en la cola se agrega el elemento y se aumenta el indice de escritura
	memcpy(q->data+index,item,q->element_size);
	q->index_head = (q->index_head + 1) % q->queue_size;
	q->blocked_task = NULL;
}

/*************************************************************************************************
	 *  @brief Funcion para recibir un dato desde una cola
     *
     *  @details
     *   Si la cola estaba bloqueando una tarea, esta pasa al estado ready.
     *   Si la cola esta vacia, la tarea que intenta leer un dato se bloquea.
     *   Si la cola no esta vacia, se lee un dato y se aumenta el indice de lectura
     *
	 *  @param		q 		Puntero a la estructura de la cola a leer.
	 *  			item 	Dato leido de la cola.
	 *  @return     None
***************************************************************************************************/
void os_QueueReceive(os_queue* q, void* item) {
	uint16_t index;
	taskStruct * current_task;

	index = q->index_tail * q->element_size;

	// Si la cola esta llena y bloqueando a una tarea que quiera escribir, esa tarea es desbloqueada
	if (((((q->index_head +1) % q->queue_size) == q->index_tail) && q->blocked_task!=NULL) && (q->blocked_task->state==TASK_BLOCKED)) {
		q->blocked_task->state = TASK_READY;
		add_ready_task(q->blocked_task);
		q->blocked_task = NULL;
		// Si la API fue llamada desde una interrupcion se debe forzar un re-schedule
		if (os_getState() == OS_IRQ_RUN)
			os_setScheduleFromISR(true);
	}

	// Si se quiere leer una cola desde un ISR y esta vacia, la operacion es abortada (no se puede bloquear un handler)
	if ((os_getState() == OS_IRQ_RUN) && (q->index_head == q->index_tail)) {
		os_setError(ERR_OS_QUEUE_EMPTY_ISR, os_QueueReceive);
		return;
	}

	// Si la cola esta vacia, la tarea actual se bloquea
	while(q->index_head == q->index_tail){
		os_enterCritical();
	    // -----------------------------------------------------------------------------
		// Inicio de seccion critica
	    // -----------------------------------------------------------------------------
		current_task = os_getCurrentTask();
		current_task->state = TASK_BLOCKED;
		q->blocked_task = current_task; // El puntero a la estructura de la tarea bloqueada se guarda dentro de la estructura de la cola
		// -----------------------------------------------------------------------------
		os_exitCritical();
		// Se fuerza un rescheduling del sistema
		os_forceSchedule();
	}
	// Si la cola no esta vacia, se lee un dato y se aumenta el indice de lectura
	memcpy(item,q->data+index,q->element_size);
	q->index_tail = (q->index_tail + 1) % q->queue_size;
	q->blocked_task = NULL;
}
