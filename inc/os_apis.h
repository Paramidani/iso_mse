/*
 * os_apis.h
 *
 *  Created on: Aug 2, 2021
 *      Author: mparamidani
 */

#ifndef PROJECTS_ISO_MSE_INC_OS_APIS_H_
#define PROJECTS_ISO_MSE_INC_OS_APIS_H_

#include "os_core.h"

/********************************************************************************
 * Definicion de la estructura para cada semaforo
 *******************************************************************************/
typedef struct _os_semaphore  {
	bool taken;
	taskStruct *blocked_task;
} os_semaphore;


/********************************************************************************
 * Definicion de la estructura para cada cola
 *******************************************************************************/
typedef struct _os_queue  {
	uint8_t data[QUEUE_HEAP_SIZE];
	taskStruct* blocked_task;
	uint16_t index_head;
	uint16_t index_tail;
	uint16_t element_size;
	uint16_t queue_size; 	// queue_size * element_size < QUEUE_HEAP_SIZE
} os_queue;

/*==================[definicion de prototipos]=================================*/
extern void add_ready_task(taskStruct* t);
extern void errorHook(void *caller);

// Delay API
void os_ApiDelay(uint32_t time_ms);

// Semaphore API
void os_SemaphoreInit(os_semaphore* sem);
void os_SemaphoreTake(os_semaphore* sem);
void os_SemaphoreGive(os_semaphore* sem);

// Queue API
void os_QueueInit(os_queue* q, uint16_t element_size, uint16_t queue_size);
void os_QueueSend(os_queue* q, void* item);
void os_QueueReceive(os_queue* q, void* item);

#endif /* PROJECTS_ISO_MSE_INC_OS_APIS_H_ */
