/*
 * os_core.h
 *
 *  Created on: 21 Junio 2021
 *      Author: mparamidani
 */

#ifndef PROJECTS_ISO_MSE_INC_OS_CORE_H_
#define PROJECTS_ISO_MSE_INC_OS_CORE_H_

#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "board.h"


/************************************************************************************
 * 			Tamaño del stack predefinido para cada tarea expresado en bytes
 ***********************************************************************************/

#define STACK_SIZE 256

//----------------------------------------------------------------------------------



/************************************************************************************
 * 	Posiciones dentro del stack frame de los registros que conforman el stack frame
 ***********************************************************************************/

#define XPSR			1
#define PC_REG			2
#define LR				3
#define R12				4
#define R3				5
#define R2				6
#define R1				7
#define R0				8
#define LR_PREV_VALUE	9
#define R4				10
#define R5				11
#define R6				12
#define R7				13
#define R8				14
#define R9				15
#define R10 			16
#define R11 			17

//----------------------------------------------------------------------------------


/************************************************************************************
 * 			Valores necesarios para registros del stack frame inicial
 ***********************************************************************************/

#define INIT_XPSR 	1 << 24			// xPSR.T = 1
#define EXEC_RETURN	0xFFFFFFF9		// retornar a modo thread con MSP, FPU no utilizada

//----------------------------------------------------------------------------------


/************************************************************************************
 * 						Definiciones varias
 ***********************************************************************************/
#define SYSTICK_TIME_US			1000

#define STACK_FRAME_SIZE		8
#define FULL_STACKING_SIZE 		17	// 16 core registers + valor previo de LR

#define N_PRIORITIES 			4

#define MAX_TASK_NAME_LEN		20	// Tamaño array correspondiente al nombre
#define MAX_TASK_COUNT			8	// Cantidad maxima de tareas para este OS
#define MAX_TASK_COUNT_W_IDLE	MAX_TASK_COUNT+1	// Cantidad maxima de tareas mas tarea idle

#define QUEUE_HEAP_SIZE 		160 // Tamano maximo de cada cola en bytes

/*==================[definicion codigos de error de OS]=================================*/
#define ERR_OS_MAX_TASK			-1 	// Error que indica que se alcanzo el numero maximo de tareas.
#define ERR_OS_READY_LIST 		-2  // Indica un error con el manejo de la lista de tareas en estado TASK_READY.
#define ERR_OS_QUEUE 			-3  // Indica un error en el manejo de colas.
#define ERR_OS_IRQ_HANDLER 		-4  // Indica un error en el manejo de interrupciones.
#define ERR_OS_DELAY_FROM_ISR   -5  // Indica que se quiso ejecutar la funcion os_ApiDelay desde el handler de una interrupcion.
#define ERR_OS_SEMTAKE_FROM_ISR -6  // Indica que se quiso ejecutar la funcion os_SemaphoreTake desde el handler de una interrupcion.
#define ERR_OS_QUEUE_FULL_ISR   -7  // Indica que se quiso escribir una cola llena desde una interrupcion.
#define ERR_OS_QUEUE_EMPTY_ISR  -8  // Indica que se quiso leer una cola vacia desde una interrupcion.


/*==================[definicion de datos para el OS]=================================*/

/********************************************************************************
 * Definicion de los estados posibles para las tareas
 *******************************************************************************/
typedef enum _taskState  {
	TASK_READY,
	TASK_RUNNING,
	TASK_BLOCKED
} taskState;

/********************************************************************************
 * Definicion de los niveles de prioridad posibles para las tareas
 *******************************************************************************/
typedef enum taskPriority {
	TASK_PRIORITY_HIGHEST,
	TASK_PRIORITY_HIGH,
	TASK_PRIORITY_MEDIUM,
	TASK_PRIORITY_LOW,
	TASK_PRIORITY_IDLE
}taskPriority;

/********************************************************************************
 * Definicion de los estados posibles de nuestro OS
 *******************************************************************************/

typedef enum _osState  {
	OS_NORMAL_RUN,
	OS_FROM_RESET,
	OS_IRQ_RUN
} osState;

/********************************************************************************
 * Definicion de la estructura para cada tarea
 *******************************************************************************/
typedef struct _taskStruct  {
	uint32_t stack[STACK_SIZE/4];
	uint32_t stack_pointer;
	void *entry_point;
	taskState state;
	taskPriority priority;
	uint8_t id;
	char name[MAX_TASK_NAME_LEN];
	uint32_t blocked_ticks;
} taskStruct;

/********************************************************************************
 * Definicion de la estructura de control para el sistema operativo
 *******************************************************************************/
typedef struct _osControl  {
	taskStruct *tasks_list[MAX_TASK_COUNT_W_IDLE]; 	// Array de punteros a tareas.
	int32_t error;									// Variable que contiene el ultimo error generado.
	uint8_t cant_tasks;								// Cantidad de tareas definidas por el usuario para cada prioridad.
	osState state;									// Informacion sobre el estado del OS.
	taskStruct *current_task;						// Definicion de puntero para tarea actual.
	taskStruct *next_task;							// Definicion de puntero para tarea siguiente.
	int16_t critical_counter; 						// Contador de secciones criticas solicitadas.
	bool scheduler_irq; 							// Flag para ejecutar el scheduler al volver de una interrupcion.
} osControl;

/*==================[definicion de prototipos]=================================*/

void os_initTask(void *entryPoint, char *name, taskPriority priority, taskStruct *task);
void os_init(void);
void os_setError(int32_t err_type, void* caller);
int32_t os_getError(void);
uint8_t os_getCantTasks(void);
osState os_getState(void);
void os_setState(osState st);
taskStruct * os_getCurrentTask(void);
bool os_getScheduleFromISR(void);
void os_setScheduleFromISR(bool val);
void os_forceSchedule(void);
void os_enterCritical(void);
void os_exitCritical(void);

#endif /* PROJECTS_ISO_MSE_INC_OS_CORE_H_ */
