/*
 * os_irq.h
 *
 *  Created on: Aug 16, 2021
 *      Author: mparamidani
 */

#ifndef PROJECTS_ISO_MSE_INC_OS_IRQ_H_
#define PROJECTS_ISO_MSE_INC_OS_IRQ_H_

#include "os_core.h"
#include "os_apis.h"
#include "board.h"
#include "cmsis_43xx.h"

#define N_IRQ	53 // Numero de interrupciones disponibles

// Handler de irq del usuario
typedef void (*usr_irq_handler_t)(void);

bool os_attachIrq(LPC43XX_IRQn_Type irq_n, usr_irq_handler_t usr_irq_handler);
bool os_detachIrq(LPC43XX_IRQn_Type irq_n);

#endif /* PROJECTS_ISO_MSE_INC_OS_IRQ_H_ */
